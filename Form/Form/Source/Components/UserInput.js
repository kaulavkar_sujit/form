
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TextInput } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';

import { SCREEN_WIDTH } from '../Helper/Constant';

export default class UserInput extends Component {

    constructor(props) {
        super(props)
        this.state = {
            textInputStatus: 'untouched',
            isSecureText: this.props.secureTextEntry
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    ref="textInput"
                    disableFullscreenUI={true}
                    style={styles.input}
                    placeholder={this.props.placeholder}
                    placeholderTextColor="#A9A9A9"
                    secureTextEntry={this.state.isSecureText}
                    onChangeText={(text) => this.onTextChange(text)}
                    onSubmitEditing={(event) => this.onEndEditing(event)}
                    placeholderTextColor="darkgray"
                    underlineColorAndroid="transparent"/>

                {this.props.secureTextEntry ?

                    <Entypo
                        name={this.state.isSecureText ? "eye-with-line" : "eye"}
                        color="#2e2e1f"
                        size={20}
                        style={styles.rightImageStyle}
                        backgroundColor={"transparent"}
                        onPress={() => this.showPassword()}
                    /> : null}

                <View style={styles.separatorView} />
            </View>
        );
    }

    onTextChange = (text) => {
            if (this.props.onTextChange != null) {
                this.props.onTextChange(text)
            }        
    }

    onEndEditing = (event) => {

        if (event) {
            if (this.props.onEditingEnd != null) {
                this.props.onEditingEnd(event)
            }
        }
    }

    showPassword() {
        this.setState({
            isSecureText: !this.state.isSecureText
        })
    }

    getText() {
        return this.refs.textInput._lastNativeText
    }

    clearTextInput() {
        this.refs.textInput.value = ""
    }
}

UserInput.propTypes = {
    placeholder: PropTypes.string.isRequired,
    secureTextEntry: PropTypes.bool
};

UserInput.defaultProps = {
    placeholder: "",
    secureTextEntry: false
}

const styles = StyleSheet.create({
    input: {
        fontSize: 15,
        height: 40,
        minWidth: SCREEN_WIDTH * 0.75,
        maxWidth: SCREEN_WIDTH * 0.75,
        color: 'black',
    },
    container: {
        height: 40,
        marginHorizontal: SCREEN_WIDTH * 0.09,
    },
    rightImageStyle: {
        position: 'absolute',
        zIndex: 100,
        width: 22,
        height: 22,
        right: 0,
        top: 9,
    },
    separatorView: {
        height: 1,
        margin: 0,
        backgroundColor: '#C8C8C8'
    }
});