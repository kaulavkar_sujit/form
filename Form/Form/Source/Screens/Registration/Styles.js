import {
    StyleSheet
} from 'react-native';

import { TOP_BAR_HEIGHT } from '../../Helper/Constant';

export const Styles = StyleSheet.create({
    container: {
        flex: 1
    },
    subContainer: {
        top: TOP_BAR_HEIGHT,
        height: 230,
        justifyContent: 'space-between'
    },
    submitBtnStyle: {
        position: 'absolute',
        bottom: 30,
        alignSelf: 'center'
    }
})
