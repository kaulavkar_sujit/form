import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

import {
    TOP_BAR_HEIGHT
} from '../Helper/Constant';

import Ionicons from 'react-native-vector-icons/Ionicons';

export default class TopBar extends Component {

    render() {
        return (
            <View style={styles.container}>
                {this.showTitle()}
                {this.showBackButton()}
            </View>
        )
    }

    showTitle() {
        return (
            <View style={styles.titleViewStyle}>
                <Text style={styles.titleStyle}>
                    {this.props.title}
                </Text>
            </View>
        )
    }

    showBackButton() {
        if (this.props.isBackButtonNeeded == true) {
            return (
                <View style={styles.leftViewStyle}>
                    <Ionicons
                        name="ios-arrow-round-back"
                        color="white"
                        size={45}
                        backgroundColor={"transparent"}
                        onPress={this.props.backButtonAction}/>
                </View>
            )
        }

        return null
    }
}

const styles = StyleSheet.create({
    container: {
        height: TOP_BAR_HEIGHT,
        backgroundColor:"red"
    },
    titleViewStyle:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    titleStyle: {
        fontSize: 22,
        alignSelf:'center',
        color: "white"              
    },
    leftViewStyle: {
        left:10,
        height: TOP_BAR_HEIGHT,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'flex-end'
    }
})