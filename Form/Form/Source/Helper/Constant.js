
import { Dimensions, Platform } from 'react-native';

export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;
export const IS_IOS = Platform.OS === 'ios'

export const TOP_BAR_HEIGHT = 64;

export const APP_NAME = "Form"

export const OK_TEXT = "OK"