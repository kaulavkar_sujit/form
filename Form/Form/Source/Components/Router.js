import React, { Component } from 'react'

import {
    LOGIN
} from '../Helper/FilePath';

import { createAppContainer, createStackNavigator } from 'react-navigation';

const LandingStackNavigation = createStackNavigator({
        LoginScreen: {
            screen: LOGIN.LOGIN_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        },
        RegistrationScreen: {
            screen: LOGIN.REGISTRATION_SCREEN,
            navigationOptions: {
                gesturesEnabled: false,
            },
        }
    },
    {
        headerMode: 'none',
        initialRouteName: 'LoginScreen'
    })

const AppContainer = createAppContainer(LandingStackNavigation);

export default AppContainer;