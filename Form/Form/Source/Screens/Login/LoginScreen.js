
import React, { Component } from 'react';
import {
    View,
    Alert,
    Text,
    TouchableOpacity
} from 'react-native';

import {
    COMPONENTS,
    HELPER
} from '../../Helper/FilePath';

import { Styles } from './Styles';
import { APP_NAME, OK_TEXT } from '../../Helper/Constant';

export default class LoginScreen extends Component {

    constructor() {
        super()
        onEndEditing = this.onEndEditing.bind(this)
        onSubmitPress = this.onSubmitPress.bind(this)
    }

    componentDidMount() {
        HELPER.FIREBASE_HANDLER.getInstance()
    }

    remoteLoginCall() {

        // Check for internet connection
        
        const username = this.refs.username.getText()
        const password = this.refs.password.getText()

        // we need to compare both user name and password
        HELPER.FIREBASE_HANDLER.getInstance().isUserExist(username).then((result) => {
            if (result == null) {
                this.showAlert("User not exists")
            }
            else {
                this.showAlert("Login successful")
            }
        })
    }

    render() {
        return (
            <View style={Styles.container}>
                <COMPONENTS.TOP_BAR
                    ref="topbarRef"
                    title='Login'/>

                <View style={Styles.subContainer}>
                    <COMPONENTS.USER_INPUT
                        ref="username"
                        placeholder="Username" />
                    <COMPONENTS.USER_INPUT
                        ref="password"
                        placeholder="Password"
                        secureTextEntry={true}
                        onEditingEnd={(text) => this.onEndEditing(text)} />

                    <TouchableOpacity style={Styles.registerBtnStyle} onPress={() => this.registerButtonAction()}>
                        <Text style={{
                            color: 'black',
                            paddingLeft: 5,
                            fontSize: 14,
                        }}>Register Now</Text>
                    </TouchableOpacity>
                </View>

                <View style={Styles.submitBtnViewStyle}>
                    <COMPONENTS.SUBMIT_BUTTON
                        title="Login"
                        onPress={() => this.onSubmitPress()}/>
                </View>

            </View>
        );
    }

    onEndEditing(text) {
        // do something if needed
    }

    onSubmitPress() {

        const username = this.refs.username.getText()
        const password = this.refs.password.getText()

        let validator = new HELPER.VALIDATOR()
        let errorMessage = validator.validateUserName(username)
        if (errorMessage.length != 0) {
            this.showAlert(errorMessage)
            return;
        }
        
        errorMessage = validator.validatePassword(password)
        if (errorMessage.length != 0) {
            this.showAlert(errorMessage)
            return;
        }

        this.remoteLoginCall()
    }

    registerButtonAction() {
        this.props.navigation.navigate("RegistrationScreen")
    }

    showAlert(message) {
        Alert.alert(APP_NAME, message,[
                {
                    text: OK_TEXT, onPress:() => {}
                }], 
                { cancelable: false })
    }
}