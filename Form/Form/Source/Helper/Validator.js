
export default class Validator {

    validateUserName(text) {
        let isValid = this.isEmpty(text)
        if (isValid == false) {
            return "Enter User name"
        }
        return ""
    }

    validateEmailAddress(text) {
        let isValid = this.isEmpty(text)
        if (isValid == false) {
            return "Enter Email Address"
        }

        var reg = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        isValid = this.validateForRegex(reg, text)
        if (isValid == false) {
            return "Email Address is not valid"
        }

        return ""
    }

    validatePassword(text) {
        let isValid = this.isEmpty(text)
        if (isValid == false) {
            return "Enter Password"
        }

        isValid = this.validateLength(8, text)
        if (isValid == false) {
            return "Password should be greater than 8 characters"
        }

        // Add regex to match pattern

        return ""
    }

    validateConfirmPassword(password, confirmPassword) {
        let isValid = this.isEmpty(confirmPassword)
        if (isValid == false) {
            return "Enter Confirm Password"
        }

        isValid = this.validateLength(8, confirmPassword)
        if (isValid == false) {
            return "Confirm Password should be greater than 8 characters"
        }

        // Add regex to match pattern

        if (confirmPassword != password) {
            return "Confirm Password is not matching with New Password"
        }

        return ""
    }

    removeSpaces(text) {        
        return text.trim()
    }

    isEmpty(text) {        
        if (text == undefined || text == null) {
            return false
        }

        text = this.removeSpaces(text)
        return text.length > 0 ? true : false
    }

    validateLength(numberOfChar, text) {
        text = this.removeSpaces(text)
        return text.length >= numberOfChar ? true : false
    }

    validateForRegex(reg, text) {
        return reg.test(text)
    }

}