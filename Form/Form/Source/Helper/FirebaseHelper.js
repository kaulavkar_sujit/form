import {
} from 'react-native';

import firebase from 'react-native-firebase';

export default class FirebaseHelper {

    static instance = null;
    static createInstance() {
        var object = new FirebaseHelper();
        return object;
    }

    static getInstance() {
        if (!FirebaseHelper.instance) {
            FirebaseHelper.instance = FirebaseHelper.createInstance();
        }
        return FirebaseHelper.instance;
    }

    constructor() {

        var config = {
            databaseURL: "https://form-d40dd.firebaseio.com",
            projectId: "form-d40dd",
        };
        if (!firebase.apps.length) {
            firebase.initializeApp(config);
        }
    }

    // This will create one entry into Firebase DB.
    register(username, email, password) {        
        firebase.database().ref('Users/' + username + "/").set({
            username: username,
            email: email,
            password: password,
        }).then((data) => {
            //success callback
            console.log('data ', data)
        }).catch((error) => {
            //error callback
            console.log('error ', error)
        })
    }

    isUserExist(username) {    
        return new Promise((resolve) => {            
            firebase.database().ref('Users/' + username).once('value', function (snapshot) {
                resolve(snapshot.val())
            });
        })
    }
}