
/************************************************************************/

import UserInput from '../Components/UserInput';
import Spinner from '../Components/Spinner';
import TopBar from '../Components/TopBar';
import SubmitButton from '../Components/SubmitButton';

export const COMPONENTS = {
    USER_INPUT: UserInput,
    SPINNER: Spinner,
    TOP_BAR: TopBar,
    SUBMIT_BUTTON: SubmitButton
}

/************************************************************************/

import Validator from '../Helper/Validator';
import FirebaseHelper from './FirebaseHelper';

export const HELPER = {
    VALIDATOR: Validator,
    FIREBASE_HANDLER: FirebaseHelper
}

/************************************************************************/

import LoginScreen from '../Screens/Login/LoginScreen';
import RegistrationScreen from '../Screens/Registration/RegistrationScreen';

export const LOGIN = {
    LOGIN_SCREEN: LoginScreen,
    REGISTRATION_SCREEN: RegistrationScreen
}

/************************************************************************/