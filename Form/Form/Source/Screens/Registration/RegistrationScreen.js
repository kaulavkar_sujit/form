
import React, { Component } from 'react';
import {
    View,
    Alert
} from 'react-native';

import {
    COMPONENTS,
    HELPER
} from '../../Helper/FilePath';

import { Styles } from './Styles';
import { APP_NAME, OK_TEXT } from '../../Helper/Constant';

export default class RegistrationScreen extends Component {

    constructor() {
        super()
        onEndEditing = this.onEndEditing.bind(this)
        onSubmitPress = this.onSubmitPress.bind(this)
        handleSuccess = this.handleSuccess.bind(this)
    }

    remoteRegistrationCall() {

        // Check for internet connection
        
        const username = this.refs.username.getText()

        HELPER.FIREBASE_HANDLER.getInstance().isUserExist(username).then((result) => {
            if (result == null) {
                const email = this.refs.email.getText()
                const password = this.refs.password.getText()
                const request = HELPER.FIREBASE_HANDLER.getInstance().register(username, email, password)
                if (request) {
                    request.then((resolve) => {
                        console.log("resolve");
                        
                        if (resolve) {
                            this.handleSuccess()
                        }
                        else {
                            // something went wrond
                        }
                    })
                }
            }
            else {
                this.showAlert("User already exists")
            }
        })
    }

    handleSuccess() {
        this.showAlert("Registration successful")
    }

    render() {
        return (
            <View style={Styles.container}>
                <COMPONENTS.TOP_BAR
                    ref="topbarRef"
                    title='Register'
                    isBackButtonNeeded={true}
                    backButtonAction={() => {
                        this.props.navigation.goBack()
                    }}
                />

                <View style={Styles.subContainer}>
                    <COMPONENTS.USER_INPUT
                        ref="username"
                        placeholder="Username" />

                    <COMPONENTS.USER_INPUT
                        ref="email"
                        placeholder="Email Address" />

                    <COMPONENTS.USER_INPUT
                        ref="password"
                        placeholder="Password"
                        secureTextEntry={true}
                        onEditingEnd={(text) => this.onEndEditing(text)} />
                    <COMPONENTS.USER_INPUT
                        ref="confirmPassword"
                        placeholder="Confirm Password"
                        secureTextEntry={true}
                        onEditingEnd={(text) => this.onEndEditing(text)} />
                </View>

                <View style={Styles.submitBtnStyle}>
                    <COMPONENTS.SUBMIT_BUTTON
                        title="Register"
                        onPress={() => this.onSubmitPress()} />
                </View>

            </View>
        );
    }

    onEndEditing(text) {
        // do something if needed
    }

    onSubmitPress() {

        const username = this.refs.username.getText()
        const email = this.refs.email.getText()
        const password = this.refs.password.getText()
        const confirmPassword = this.refs.confirmPassword.getText()

        let validator = new HELPER.VALIDATOR()

        let errorMessage = validator.validateUserName(username)
        if (errorMessage.length != 0) {
            this.showAlert(errorMessage)
            return;
        }

        errorMessage = validator.validateEmailAddress(email)
        if (errorMessage.length != 0) {
            this.showAlert(errorMessage)
            return;
        }

        errorMessage = validator.validatePassword(password)
        if (errorMessage.length != 0) {
            this.showAlert(errorMessage)
            return;
        }

        errorMessage = validator.validateConfirmPassword(password, confirmPassword)
        if (errorMessage.length != 0) {
            this.showAlert(errorMessage)
            return;
        }

        this.remoteRegistrationCall()
    }

    registerButtonAction() {
        this.props.navigation.navigate("RegistrationScreen")
    }

    showAlert(message) {
        Alert.alert(APP_NAME, message, [
            {
                text: OK_TEXT, onPress: () => { }
            }],
            { cancelable: false })
    }
}