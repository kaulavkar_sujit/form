import {
    StyleSheet
} from 'react-native';

import { TOP_BAR_HEIGHT } from '../../Helper/Constant';

export const Styles = StyleSheet.create({
    container: {
        flex: 1
    },
    subContainer: {
        top: TOP_BAR_HEIGHT,
        height: 150,
        justifyContent: 'space-between'
    },
    registerBtnStyle: {
        alignSelf: 'center',
        bottom: 0,
        height: 40,
        width: 100
    },
    submitBtnViewStyle: {
        position: 'absolute',
        bottom: 30,
        alignSelf: 'center'
    }
})


